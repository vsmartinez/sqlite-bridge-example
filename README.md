# README #

This project show you how to user sqlite-bridge library allowing incoming SQL statement from external sqlite-bridge-client

### How to use it? ###
* Add the sqlite-bridge.jar library to your project classpath
* Create sqlite-bridge instance in your context, you must to provide your SQLiteOpenHelper instance.
```
#!java


public class SqliteBridgeApplication extends Application {

	private SqliteBridge bridge;

	@Override
	public void onCreate() {
		super.onCreate();

		bridge = new SqliteBridge(new DataBaseHelper(this));
		bridge.start();
	}

	@Override
	public void onTerminate() {
		bridge.stop();
	}
}

```
* After *start* method has been called you are ready to receive SQL statements from [sqlite-client](https://bitbucket.org/vsmartinez/sqlite-bridge-client) shell.

### Do not extends SQLiteOpenHelper? ###
Use *SQLiteDelegator* 
```
#!java
 SqliteBridge sql = new SqliteBridge(new SQLiteDelegator() {

            @Override
            public Cursor rawQuery(String sql) {
                return sqliteCipher.getReadableDatabase().rawQuery(sql, null);
            }

            @Override
            public void execSQL(String sql) {
                sqliteCipher.getWritableDatabase().execSQL(sql);
            }
        });

 sql.start();
```

# License #
```
Copyright 2015 Vidal Santiago Martinez. vidalsantiagomartinez at gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```