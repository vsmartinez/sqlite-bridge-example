package com.github.sqlitebridge;

import android.app.Application;

import com.github.sqlitebridge.storage.DataBaseHelper;

public class SqliteBridgeApplication extends Application {

	private SqliteBridge bridge;

	@Override
	public void onCreate() {
		super.onCreate();

		bridge = new SqliteBridge(new DataBaseHelper(this));
		bridge.start();
	}

	@Override
	public void onTerminate() {
		bridge.stop();
	}
}
