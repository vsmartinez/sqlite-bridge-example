package com.github.sqlitebridge.storage;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.github.sqlitebridge.R;

public class DataBaseHelper extends SQLiteOpenHelper {

	/*
	 * In memory database
	 */
	public static final String DATABASE_NAME = null;

	/*
	 * Database password for sqlcipher
	 */
	private static final String DATABASE_PWD = "s3cr3t";

	public static final int DATABASE_VERSION = 1;

	private Context context;

	public DataBaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	public void onCreate(final SQLiteDatabase db) {
		db.beginTransaction();
		try {
			execMultipleSQL(db,
					context.getResources()
							.getStringArray(R.array.create_schema));
			db.setTransactionSuccessful();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	/*
	 * If you are running with Sqlcipher, uncomment this method
	 */
	// public SQLiteDatabase getWritableDatabase() {
	// return getWritableDatabase(DATABASE_PWD);
	// }

	/*
	 * If you are running with Sqlcipher, uncomment this method
	 */
	// public SQLiteDatabase getReadableDatabase() {
	// return getReadableDatabase(DATABASE_PWD);
	// }

	public static void execMultipleSQL(SQLiteDatabase db, String[] sql) {
		for (String s : sql)
			if (s.trim().length() > 0)
				db.execSQL(s);
	}
}
